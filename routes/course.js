const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require('../auth')

// creating a course
router.post("/", auth.verify,  (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Not Authorized");
	}

	
});


// retrieving all courses
router.get('/all', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	courseController.getAllCourses(data).then(resultFromController => res.send(resultFromController));
});

// retrieve all active course(s)
router.get('/', (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// retrieve a specific course

router.get('/:courseId', (req, res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// update course

router.put('/:courseId', auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// archiving course

router.put("/:courseId/archive", auth.verify, (req,res) => {
			let isAdmin = auth.decode(req.headers.authorization).isAdmin
			console.log(isAdmin)
			if (isAdmin) {
				courseController.archiveCourse(req.params, req.body).then(resultFromController => (res.send(`${resultFromController}. User is admin, course successfully archived!`)))
				
			}
			else {
				res.send(`User is not admin, cannot archive course.`)
			}
		})





module.exports = router;
