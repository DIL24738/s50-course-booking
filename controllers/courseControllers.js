const Course = require("../models/Course");

// create course
module.exports.addCourse = (reqBody) =>{


	let newCourse = new Course ({
		name: reqBody.name, 
		description: reqBody.description,
		price: reqBody.price 
		
	})

	return newCourse.save().then((course, error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})



}





// retrieve all courses

module.exports.getAllCourses = async (user) => {
	// console.log(user)

	if(user.isAdmin === true){

		return Course.find({}).then(result => {

			return result
		})
	} else {

		return `${user.email} is not authorized`
	}

}

// retrieve all active courses

module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result
	})
}

// retrieve a specific course

module.exports.getCourse = (reqParams) => {

console.log(reqParams)

// with field projection
	return Course.findById(reqParams.courseId).then(result => {
// console.log(result)

		return result
	})
}

// update a course

module.exports.updateCourse = (reqParams, reqBody) => {

console.log(reqParams)
console.log(reqBody)

	let updatedCourse = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		if(error){
			return false
		} else {
			return course
		}
	})
}

// archiving course

module.exports.archiveCourse = (reqParams, reqBody) => {
			let archivedCourse = {
				isActive: reqBody.isActive
			}
			
			return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		}









